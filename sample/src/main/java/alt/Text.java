package alt;

public class Text extends Root {
    protected String s;
    public Text(String s) {
        this.s = s;
    }
    public Text[] split(String regex) {
        String[] split = this.s.split(regex);
        Text[] result = new Text[split.length];
        for(int i=0; i<result.length; i++) {
            result[i] = new Text(split[i]);
        }
        return result;
    }
    @Override
    public String toString() {
        return "Text(" + this.s +")";
    }
}
