package ult01.generator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

public class GenerateCpp {
    StringBuilder sb = new StringBuilder();
    public void generate(JSONObject jo) {
        this.generateCompilationUnit(jo, 0);
        System.out.print(sb.toString());
    }
    void println(Object s, int indentLevel) {
        sb.append("*");
        for(int i=0; i<indentLevel; i++) {
            sb.append("  ");
        }
        this.sb.append(s);
        this.sb.append("\n");
    }
    void printLineComment(String s, int indentLevel) {
        this.println("// " + s, indentLevel);
    }
    void generateCompilationUnit(JSONObject jo, int indentLevel) {
        this.printLineComment(jo.getString("!"), indentLevel);
        JSONArray types = jo.getJSONArray("types");
        for(int i=0; i<types.length(); i++) {
            System.out.println(i);
            this.generateType(types.getJSONObject(i), indentLevel);
        }
    }
    void generateType(JSONObject jo, int indentLevel) {
        String tag = jo.getString("!");
        this.printLineComment(tag, indentLevel);
        String typeKind;
        if(tag == "com.github.javaparser.ast.body.ClassDeclaration") {
            typeKind = "class";
        } else if (tag == "com.github.javaparser.ast.body.InterfaceDeclaration") {
            typeKind = "interface";
        } else {
            typeKind = "?";
        }
        String modifiers = jo.getJSONArray("modifiers").join(" ").replaceAll("\"", "").toLowerCase();
        if(!modifiers.isEmpty()) modifiers += " ";
        this.println(String.format("%s%s %s {", modifiers, typeKind, jo.getJSONArray("name").getString(0)), indentLevel);
        JSONArray members = jo.getJSONArray("members");
        for(int i=0; i<members.length(); i++) {
            JSONObject member = members.getJSONObject(i);
            this.generateTypeMember(member, indentLevel + 1);
        }
        this.println("}", indentLevel);
    }
    void generateTypeMember(JSONObject jo, int indentLevel) {
        this.printLineComment(jo.getString("!"), indentLevel);
        switch(jo.getString("!")) {
            case "com.github.javaparser.ast.body.FieldDeclaration":
                generateField(jo, indentLevel);
                break;
            case "com.github.javaparser.ast.body.MethodDeclaration":
                break;
            case "com.github.javaparser.ast.body.ClassDeclaration":
            case "com.github.javaparser.ast.body.InterfaceDeclaration":
                generateType(jo, indentLevel);
        }
    }
    void generateField(JSONObject jo, int indentLevel) {
        this.printLineComment(jo.getString("!"), indentLevel);
        String modifiers = jo.getJSONArray("modifiers").join(" ").replaceAll("\"", "").toLowerCase();
        if(!modifiers.isEmpty()) modifiers += " ";
        JSONArray variables = jo.getJSONArray("variables");
        for(int i=0; i<variables.length(); i++) {
            JSONObject variable = variables.getJSONObject(i);
            String typeString = this.typeString(variable.getJSONObject("type"));
            System.err.println(variable.toString(2));
            this.println(String.format("%s%s %s;", modifiers, typeString, variable.getJSONArray("name").getString(0)), indentLevel);
        }
    }
    String typeString(JSONObject jo) {
        String tag = jo.getString("!");
        switch(tag) {
            case "<primitive>":
                return jo.getString("*").toLowerCase();
            case "<array>":
                String unit = typeString(jo.getJSONObject("*"));
                return unit + "[]";
            case "<identifiers>":
                return this.typeStringForIdentifiers(jo);
        }
        return jo.toString();
    }
    String typeStringForIdentifiers(JSONObject jo) {
        String result = jo.getString("*");
        if(jo.has("?")) {
            JSONArray components = jo.getJSONArray("?");
            result += "<";
            for(int i=0; i<components.length(); i++) {
                if(i>0) result += ", ";
                result += this.typeString(components.getJSONObject(i));
            }
            result += ">";
        }
        return result;
    }
}
