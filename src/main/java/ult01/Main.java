package ult01;

import com.google.common.io.Files;
import org.json.JSONObject;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;

import ult01.decompiler.*;
import ult01.generator.GenerateCpp;

public class Main {

    public static void investigate(File jar) throws Exception {
        SourceJar sjar = new SourceJar(jar);
        List<String> classes = sjar.getClassNameList();
        System.err.println(classes);
        for (int i = 0; i < classes.size(); i++) {
            String className = classes.get(i);
            String source1 = sjar.decompile(className, Decompiler.Mode.NORMAL);
            System.err.println("[" + className + "]");
            System.err.println(source1);
            String source = sjar.decompile(className, Decompiler.Mode.ANALYZE);
            System.out.println("[" + className + "]");
            System.out.println(source);
            if(!className.equals("Main")) continue;
            //JSONObject jsonObj = sjar.parseToJson(className, Decompiler.Mode.ANALYZE, true);
            JSONObject jsonObj = sjar.parseToJson(className, Decompiler.Mode.ANALYZE);
            System.out.println(jsonObj.toString(2));
            Files.write(jsonObj.toString(2).getBytes(StandardCharsets.UTF_8), new File("ast.json"));
            GenerateCpp generateCpp = new GenerateCpp();
            generateCpp.generate(jsonObj);
            //JsonObject jsonObj = sjar.parseToGson(className, Decompiler.Mode.ANALYZE, true);
            //System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(jsonObj));
        }
    }

    public static void main(String[] args) throws Exception {
        String path = "./sample/build/libs/sample.jar";
        File jar = new File(path);
        investigate(jar);
        /*
        Class<?> stringClass = Class.forName("java.lang.String");
        Method[] methods = stringClass.getMethods();
        System.out.println(methods);
        for(int i=0; i<methods.length; i++) {
            Method method = methods[i];
            System.out.println(method.toString());
        }
        */
    }

}
