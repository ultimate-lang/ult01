package ult01.decompiler;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.serialization.JavaParserJsonSerializer;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.jd.core.v1.ClassFileToJavaSourceDecompiler;
import org.jd.core.v1.api.loader.Loader;
import org.jd.core.v1.api.loader.LoaderException;
import org.jd.core.v1.api.printer.Printer;
import org.json.JSONObject;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class SourceJar implements Loader {
    File file;
    List<String> classNames = new ArrayList<>();
    Map<String, byte[]> map = new HashMap<>();

    public SourceJar(File jarFile) {
        if (jarFile == null) throw new NullPointerException();
        this.file = jarFile;
        this.prepare();
    }

    public SourceJar(String jarFilePath) {
        if (jarFilePath == null) throw new NullPointerException();
        this.file = new File(jarFilePath);
        this.prepare();
    }

    protected void prepare() {
        try {
            try (JarFile jarFile = new JarFile(this.file)) {
                Enumeration<JarEntry> e = jarFile.entries();
                while (e.hasMoreElements()) {
                    JarEntry jarEntry = e.nextElement();
                    if (jarEntry.getName().endsWith(".class") && !jarEntry.getName().contains("$")) {
                        String className = jarEntry.getName()
                                .replaceAll("[.]class$", "")
                                .replaceAll("[/]", ".");
                        classNames.add(className);
                    }
                    InputStream is = jarFile.getInputStream(jarEntry);
                    byte[] bytes = new byte[is.available()];
                    is.read(bytes);
                    is.close();
                    this.map.put(jarEntry.getName(), bytes);
                }
            }
        } catch (Exception ex) {
            ;
        }
    }

    @Override
    public boolean canLoad(String internalName) {
        return map.containsKey(internalName + ".class");
    }

    @Override
    public byte[] load(String internalName) throws LoaderException {
        //System.err.println("LOAD: " + internalName);
        return map.get(internalName + ".class");
    }

    public List<String> getClassNameList() /*throws IOException*/ {
        return this.classNames;
    }

    public byte[] getClassBytes(String className) {
        return this.map.get(className.replaceAll("[.]", "/") + ".class");
    }

    public String decompile(String className, Decompiler.Mode mode) throws Exception {
        Printer printer = null;
        switch (mode) {
            case NORMAL:
                printer = new Decompiler.NormalPrinter();
                break;
            case ANALYZE:
                printer = new Decompiler.AnalyzePrinter();
                break;
            default:
                throw new RuntimeException();
        }
        ClassFileToJavaSourceDecompiler decompiler = new ClassFileToJavaSourceDecompiler();
        decompiler.decompile(this, printer, className.replaceAll("[.]", "/"));
        String source = printer.toString();
        return source;
    }

    public CompilationUnit parse(String className, Decompiler.Mode mode) throws Exception {
        String source = this.decompile(className, mode);
        return SourceParser.parse(source);
    }

    public JSONObject parseToJson(String className, Decompiler.Mode mode) throws Exception {
        return this.parseToJson(className, mode, false);
    }

    public JSONObject parseToJson(String className, Decompiler.Mode mode, boolean noBody) throws Exception {
        CompilationUnit cu = this.parse(className, mode);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonGenerator generator = Json.createGenerator(baos);
        JavaParserJsonSerializer serializer = new JavaParserJsonSerializer();
        serializer.serialize(cu, generator);
        String json = new String(baos.toByteArray(), "UTF-8");
        JSONObject jsonObj = new JSONObject(json);
        jsonObj = (JSONObject) OptimizeJson1.optimize(jsonObj, noBody, (mode==Decompiler.Mode.ANALYZE));
        //System.out.println(jsonObj.toString(2));
        jsonObj = (JSONObject) OptimizeJson2.optimize(jsonObj, className);
        return jsonObj;
    }

    public JsonObject parseToGson(String className, Decompiler.Mode mode, boolean noBody) throws Exception {
        CompilationUnit cu = this.parse(className, mode);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonGenerator generator = Json.createGenerator(baos);
        JavaParserJsonSerializer serializer = new JavaParserJsonSerializer();
        serializer.serialize(cu, generator);
        String json = new String(baos.toByteArray(), "UTF-8");
        JsonObject jsonObj = new JsonParser().parse(json).getAsJsonObject();
        OptimizeGson1.optimize(jsonObj, noBody, (mode==Decompiler.Mode.ANALYZE));
        OptimizeGson2.optimize(jsonObj);
        return jsonObj;
    }

}
