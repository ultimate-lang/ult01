package ult01.decompiler;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Set;

public class OptimizeGson1 {

    public static JsonElement optimize(JsonElement o, boolean noBody, boolean noImports) {
        if (o.isJsonObject()) {
            JsonObject jo = o.getAsJsonObject();
            if (noBody) {
                jo.remove("body");
            }
            if (noImports) {
                jo.remove("imports");
            }
            jo.remove("range");
            jo.remove("tokenRange");
            String[] keys = jo.keySet().toArray(new String[jo.keySet().size()]);
            for(int i=0; i<keys.length; i++) {
                String key = keys[i];
                System.out.println("KEY: " + key);
                JsonElement element = jo.get(key);
                jo.add(key, optimize(element, noBody, noImports));
            }
            return jo;
        } else if (o.isJsonArray()) {
            JsonArray ja = o.getAsJsonArray();
            for (int i = 0; i < ja.size(); i++) {
                ja.set(i, optimize(ja.get(i), noBody, noImports));
            }
            return ja;
        } else {
            return o;
        }
    }

}
