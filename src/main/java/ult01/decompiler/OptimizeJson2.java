package ult01.decompiler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.*;

public class OptimizeJson2 {

    public static Object optimize(Object o, String className) {
        if (o instanceof JSONObject) {
            JSONObject jo = (JSONObject) o;
            if (jo.has("scope") && jo.has("name")) {
                JSONObject scope = optimizeScope(jo, className);
                return scope;
            }
            if (jo.has("!")) {
                if (jo.getString("!").equals("com.github.javaparser.ast.CompilationUnit")) {
                    jo.put("path", className.replaceAll("[.]", "/") + ".class");
                    jo = reorder(jo, "!,path", className);
                    return jo;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.body.ClassOrInterfaceDeclaration")) {
                    if (jo.getBoolean("isInterface")) {
                        jo.put("!", "com.github.javaparser.ast.body.InterfaceDeclaration");
                    } else {
                        jo.put("!", "com.github.javaparser.ast.body.ClassDeclaration");
                    }
                    jo.remove("isInterface");
                    jo = reorder(jo, "!,modifiers,name", className);
                    return jo;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.body.ConstructorDeclaration")) {
                    jo = reorder(jo, "!,modifiers,name", className);
                    return jo;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.body.MethodDeclaration")) {
                    jo = reorder(jo, "!,modifiers,name", className);
                    return jo;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.body.FieldDeclaration")) {
                    jo = reorder(jo, "!,modifiers,variables", className);
                    return jo;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.expr.Name")) {
                    return optimizeQualifier(jo);
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.Modifier")) {
                    return jo.getString("keyword");
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.expr.SimpleName")) {
                    JSONArray result = new JSONArray();
                    result.put(jo.getString("identifier"));
                    return result;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.type.PrimitiveType")) {
                    JSONObject result = newJSONObject();
                    result.put("!", "<primitive>");
                    result.put("*", jo.getString("type"));
                    return result;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.type.VoidType")) {
                    JSONObject result = newJSONObject();
                    result.put("!", "<void>");
                    return result;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.type.ArrayType")) {
                    JSONObject result = newJSONObject();
                    result.put("!", "<array>");
                    result.put("*", optimize(jo.get("componentType"), className));
                    return result;
                }
                if (jo.getString("!").equals("com.github.javaparser.ast.type.ClassOrInterfaceType")) {
                    jo.remove("!");
                }
            }
            for (Iterator<String> it = jo.keys(); it.hasNext(); ) {
                String key = it.next();
                jo.put(key, optimize(jo.get(key), className));
            }
            return jo;
        } else if (o instanceof JSONArray) {
            JSONArray ja = (JSONArray) o;
            for (int i = 0; i < ja.length(); i++) {
                ja.put(i, optimize(ja.get(i), className));
            }
            return ja;
        } else {
            return o;
        }
    }

    public static JSONObject newJSONObject() {
        JSONObject result = new JSONObject();
        try {
            // https://stackoverflow.com/questions/4515676/keep-the-order-of-the-json-keys-during-json-conversion-to-csv
            Field map = result.getClass().getDeclaredField("map");
            map.setAccessible(true);//because the field is private final...
            map.set(result, new LinkedHashMap<>());
            map.setAccessible(false);//return flag
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static JSONObject reorder(JSONObject jo, String order, String className) {
        String[] array = order.split(",");
        List<String> list = Arrays.asList(array);
        JSONObject result = newJSONObject();
        for (int i = 0; i < list.size(); i++) {
            String key = list.get(i);
            if (jo.has(key)) {
                result.put(key, optimize(jo.get(key), className));
            }
        }
        for (Iterator<String> it = jo.keys(); it.hasNext(); ) {
            String key = it.next();
            if (!result.has(key)) {
                result.put(key, optimize(jo.get(key), className));
            }
        }
        return result;
    }

    public static JSONObject optimizeQualifier(JSONObject expr) {
        List<String> result = new ArrayList<>(0);
        while (expr != null) {
            result.add(expr.getString("identifier"));
            if (expr.has("qualifier")) {
                expr = expr.getJSONObject("qualifier");
            } else {
                expr = null;
            }
        }
        Collections.reverse(result);
        JSONObject obj = newJSONObject();
        obj.put("!", "<identifiers>");
        obj.put("*", result);
        return obj;
    }

    public static JSONObject optimizeScope(JSONObject scope, String className) {
        String result;
        if (!scope.has("scope")) {
            String name = optimizeGetTypeName(scope);
            result = name;
        } else {
            result = optimizeScope(scope.getJSONObject("scope"), className).getString("*");
            String name = optimizeGetTypeName(scope);
            result += ("." + name);
        }
        JSONObject obj = newJSONObject();
        obj.put("!", "<identifiers>");
        obj.put("*", result);
        if (scope.has("typeArguments")) {
            obj.put("?", optimize(scope.get("typeArguments"), className));
        }
        if (scope.has("arguments")) {
            obj.put("arguments", optimize(scope.get("arguments"), className));
        }
        return reorder(obj, "!,*,?,arguments", className);
    }

    public static String optimizeGetTypeName(JSONObject scope) {
        if (scope.has("!") && scope.getString("!").equals("com.github.javaparser.ast.expr.ThisExpr")) {
            return "this";
        }
        Object nameObj = scope.get("name");
        String name = null;
        if (nameObj instanceof String) name = (String) nameObj;
        else name = ((JSONObject) nameObj).getString("identifier");
        return name;
    }

}
