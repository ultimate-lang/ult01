package ult01.decompiler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

public class OptimizeJson1 {

    public static Object optimize(Object o, boolean noBody, boolean noImports) {
        if (o instanceof JSONObject) {
            JSONObject jo = (JSONObject) o;
            if (noBody) {
                jo.remove("body");
            }
            if (noImports) {
                jo.remove("imports");
            }
            jo.remove("range");
            jo.remove("tokenRange");
            for (Iterator<String> it = jo.keys(); it.hasNext(); ) {
                String key = it.next();
                jo.put(key, optimize(jo.get(key), noBody, noImports));
            }
            return jo;
        } else if (o instanceof JSONArray) {
            JSONArray ja = (JSONArray) o;
            for (int i = 0; i < ja.length(); i++) {
                ja.put(i, optimize(ja.get(i), noBody, noImports));
            }
            return ja;
        } else {
            return o;
        }
    }

}
