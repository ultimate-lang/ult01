package ult01.decompiler;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class OptimizeGson2 {

    public static JsonElement optimize(JsonElement o) {
        if (o.isJsonObject()) {
            JsonObject jo = o.getAsJsonObject();
            if (jo.has("scope") && jo.has("name")) {
                JsonObject scope = optimizeScope(jo);
                return scope;
            }
            if (jo.has("!")) {
                if (jo.get("!").getAsString().equals("com.github.javaparser.ast.body.ClassOrInterfaceDeclaration")) {
                    if (jo.get("isInterface").getAsBoolean()) {
                        jo.addProperty("!", "com.github.javaparser.ast.body.InterfaceDeclaration");
                    } else {
                        jo.addProperty("!", "com.github.javaparser.ast.body.ClassDeclaration");
                    }
                    jo.remove("isInterface");
                    //jo = reorder(jo, "!,name,modifiers");
                    jo = reorder(jo, "!,name");
                    return jo;
                }
                if (jo.get("!").getAsString().equals("com.github.javaparser.ast.expr.Name")) {
                    return optimizeQualifier(jo);
                }
                if (jo.get("!").getAsString().equals("com.github.javaparser.ast.Modifier")) {
                    return jo.get("keyword").getAsJsonPrimitive();
                }
                if (jo.get("!").getAsString().equals("com.github.javaparser.ast.expr.SimpleName")) {
                    JsonArray result = new JsonArray();
                    result.add(jo.get("identifier").getAsString());
                    return result;
                }
                if (jo.get("!").getAsString().equals("com.github.javaparser.ast.type.PrimitiveType")) {
                    JsonObject result = new JsonObject();
                    result.addProperty("!", "<primitive>");
                    result.addProperty("*", jo.get("type").getAsString());
                    return result;
                }
                if (jo.get("!").getAsString().equals("com.github.javaparser.ast.type.VoidType")) {
                    JsonObject result = new JsonObject();
                    result.addProperty("!", "<void>");
                    return result;
                }
                if (jo.get("!").getAsString().equals("com.github.javaparser.ast.type.ArrayType")) {
                    JsonObject result = new JsonObject();
                    result.addProperty("!", "<array>");
                    result.add("*", optimize(jo.get("componentType")));
                    return result;
                }
                /*
                if (jo.getString("!").equals("com.github.javaparser.ast.type.ClassOrInterfaceType")) {
                    jo.remove("!");
                }
                */
            }
            String[] keys = jo.keySet().toArray(new String[jo.keySet().size()]);
            for(int i=0; i<keys.length; i++) {
                String key = keys[i];
                System.out.println("KEY: " + key);
                JsonElement element = jo.get(key);
                jo.add(key, optimize(element));
            }
            return jo;
        } else if (o.isJsonArray()) {
            JsonArray ja = o.getAsJsonArray();
            for (int i = 0; i < ja.size(); i++) {
                ja.set(i, optimize(ja.get(i)));
            }
            return ja;
        } else {
            return o;
        }
    }

    public static JsonObject reorder(JsonObject jo, String order) {
        String[] array = order.split(",");
        List<String> list = Arrays.asList(array);
        JsonObject result = new JsonObject();
        String[] keys = jo.keySet().toArray(new String[jo.keySet().size()]);
        for(int i=0; i<keys.length; i++) {
            String key = keys[i];
            if(jo.has(key)) {
                result.add(key, optimize(jo.get(key)));
            }
        }
        //JsonObject rest = new JsonObject();
        //boolean exists = false;
        keys = jo.keySet().toArray(new String[jo.keySet().size()]);
        for(int i=0; i<keys.length; i++) {
            String key = keys[i];
            //exists = true;
            //rest.add(key, optimize(jo.get(key)));
            result.add(key, optimize(jo.get(key)));
        }
        //if(exists) result.add("*", rest);
        return result;
    }

    public static JsonObject optimizeQualifier(JsonObject expr) {
        List<String> result = new ArrayList<>();
        //result.add(expr.getString("identifier"));
        while (expr != null) {
            result.add(expr.get("identifier").getAsString());
            if (expr.has("qualifier")) {
                expr = expr.get("qualifier").getAsJsonObject();
            } else {
                expr = null;
            }
        }
        Collections.reverse(result);
        JsonArray resultElement = new JsonArray();
        for(int i=0; i<result.size(); i++) {
            resultElement.add(result.get(i));
        }
        JsonObject obj = new JsonObject();
        obj.addProperty("!", "<identifiers>");
        obj.add("*", resultElement);
        return obj;
    }

    public static JsonObject optimizeScope(JsonObject scope) {
        String result;
        if (!scope.has("scope")) {
            String name = optimizeGetTypeName(scope);
            result = name;
        } else {
            result = optimizeScope(scope.get("scope").getAsJsonObject()).get("*").getAsString();
            String name = optimizeGetTypeName(scope);
            result += ("." + name);
        }
        JsonObject obj = new JsonObject();
        obj.addProperty("!", "<identifiers>");
        obj.addProperty("*", result);
        if (scope.has("typeArguments")) {
            //obj.put("typeArguments", optimize(scope.get("typeArguments")));
            obj.add("?", optimize(scope.get("typeArguments")));
        }
        if (scope.has("arguments")) {
            obj.add("arguments", optimize(scope.get("arguments")));
        }
        //return reorder(obj, "!,*,typeArguments,arguments");
        return reorder(obj, "!,*,?,arguments");
    }

    public static String optimizeGetTypeName(JsonObject scope) {
        if (scope.has("!") && scope.get("!").getAsString().equals("com.github.javaparser.ast.expr.ThisExpr")) {
            return "this";
        }
        JsonElement nameObj = scope.get("name");
        String name = null;
        if (nameObj.isJsonPrimitive()) name = nameObj.getAsString();
        else name = nameObj.getAsJsonObject().get("identifier").getAsString();
        return name;
    }

}
